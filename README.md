# iconos-pasalista

Tabla con archivos de íconos usados en la UI de Pasalista

La tabla contiene las siguientes columnas

- Nombre de archivo sin sufijo de variante (string)
- URL original (string)
- Nombre correspondiente del ícono original (string)
- Idéntico al ícono en la app (boolean)
- Versión alternativa del ícono
- Notas sobre el ícono (string)
