#!/bin/bash
# Propósito: Generar los íconos en sus versiones necesarias para la IU
#            de acuerdo a lo que indica el archivo .csv
# Autor: JJSH
# Modificado: 08/04/2020
# --------------------------------------------------------------------
INPUT=./icons.csv
ICONDIR=./icons
OUTPUTDIR=./build
BKPIFS=$IFS
IFS=','

[ ! -f $INPUT ] && { echo "No se encuentra el archivo $INPUT"; exit 99;}

if [ ! -d $OUTPUTDIR ]; then
    mkdir -p $OUTPUTDIR
fi

TEMPDIR=$(mktemp -d)
LINE=1
while read iconname url original apply_mask alternative notes
do
    if [ $LINE -eq 1 ]; then # Saltarse el encabezado de las columnas
        LINE=$(($LINE + 1))
        continue
    fi
    if [ ! -z "$apply_mask" ]; then
        TEMP=$(mktemp)
        # echo "Aplicando mascara a $ICONDIR/$original"
        # Eliminar una banda del icono original
        composite -compose Out "$ICONDIR/$original" "icons/mask.png" $TEMP
        # Poner la diagonal en el espacio borrado
        composite -compose Dst_Over $TEMP "$ICONDIR/not.png" $TEMPDIR/$iconname
    else
        # echo "Redimensionando y moviendo $ICONDIR/$original"
        convert -antialias -background none $ICONDIR/$original -resize 50x50 $TEMPDIR/$iconname
    fi
    LINE=$(($LINE + 1))
done < $INPUT

for file in $TEMPDIR/*.png; do
    NAME=$(basename -s .png $file)
    echo "Copiando archivo $file a $OUTPUTDIR/$NAME_light.png"
    cp $file $OUTPUTDIR/"$NAME"_light.png
    cp $file $OUTPUTDIR/"$NAME"_dark.png
done

for filename in $OUTPUTDIR/*_light.png; do
    # Aplicar un filtro para las versiones "light"
    mogrify -alpha off -channel rgb -evaluate add 80% +channel -alpha on $filename
done

IFS=$BKPIFS
